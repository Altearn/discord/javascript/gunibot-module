const XMLHttpRequest = require("../../xmlhttprequest").XMLHttpRequest
const {getAccess} = require("./connectApi")
module.exports = {
	setWormHole
};

async function setWormHole(client, channelID, boolean) {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open( "GET", "http://185.44.81.130/android/test.php", false);
    if (!client) return console.log("GunibotAPI - missing client discord.js !");
    if (!client.channels) return console.log("GunibotAPI - missing client discord.js !");
    if (!channelID) return console.log("GunibotAPI - missing channel id !");
    if (!client.channels.cache.get(channelID)) return console.log("GunibotAPI - ChannelID is invalid !");

    let eventName = "SET_WORMHOLE"
    
    xmlHttp.setRequestHeader("TOKEN", client.token)
    xmlHttp.setRequestHeader("REQUEST", eventName)
    xmlHttp.setRequestHeader("CHANNELID", channelID)
    xmlHttp.setRequestHeader("ACCESS", (boolean ? "true" : "false"))
    xmlHttp.send();

    if (xmlHttp.responseText === "ERREUR_EVENT") return console.log("GunibotAPI - This event does not exist !");
    if (xmlHttp.responseText === "ERREUR_USE_EVENT") return console.log("GunibotAPI - You cannot use this event !");

    if (["0", "1"].includes(xmlHttp.responseText)) xmlHttp.responseText = (xmlHttp.responseText === "0" ? false : true)
    
    return xmlHttp.responseText;
}
