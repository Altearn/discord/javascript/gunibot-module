const { findBestMatch } = require('string-similarity')
const { Guild } =require('discord.js')
module.exports = {
	subCommands,
    updateClient
};
function subCommands(client, message, command, args) {
    if (!args[0] || !command.help.subcommands) return command.run[0](client, message, args)
    command.run[command.help.subcommands.indexOf(args[0])+1](client, message, args)
}

function updateClient(client) {

	/* 
	 * @param {Guild} guild 
	 * @param {String} user
     * @param {Boolean} appro
	 */
    client.getUser = async(guild, user, appro) => {
        const { cache } = client.users

        if (cache.has(user)) return cache.get(user)

        let mention = user.replace('<@','').replace('<@!').replace('>')
        if (cache.has(mention)) return cache.get(mention)

        let result = cache.find(u => u.username === user)
        if (result) return result
        
        if (appro) {
            let members = []
            let indexes = []

            cache.forEach(member => {
                members.push(member.username)
                indexes.push(member.id)
            })

            const match = findBestMatch(user, members)
            if (match.bestMatch.rating > 0) {
                const displayName = match.bestMatch.target
                return cache.get(indexes[members.indexOf(displayName)])
		    }
        }
        return null
    }

    /**
	 * 
	 * @param {Guild} guild 
	 * @param {String} channel
     * @param {Boolean} appro
	 */
     client.getChannel = async(guild, channel, appro) => {
        const { cache } = guild.channels
        if (cache.has(channel)) return cache.get(channel)

        let mention = channel.replace('<#','').replace('>')
        if (cache.has(mention)) return cache.get(mention)

        let result = cache.find(u => u.name === channel)
        if (result) return result
        if (appro) {
            let channels = []
            let indexes = []

            cache.forEach(channel => {
                channels.push(channel.name)
                indexes.push(channel.id)
            })

            const match = findBestMatch(channel, channels)
            if(match.bestMatch.rating > 0) {
                const displayName = match.bestMatch.target

                return cache.get(indexes[channels.indexOf(displayName)])
            }
        }
        return null
    }


    /**
	 * 
	 * @param {Guild} guild 
	 * @param {String} role
     * @param {Boolean} appro
	 */
    client.getRole = async(guild, role, appro) => {
        const { cache } = guild.roles
        if (cache.has(role)) return cache.get(role)

        let mention = role.replace('<@&','').replace('>')
        if (cache.has(mention)) return cache.get(mention)

        let result = cache.find(u => u.name === role)
        if (result) return result
        if (appro) {
            let roles = []
            let indexes = []

            cache.forEach(role => {
                roles.push(role.name)
                indexes.push(role.id)
            })

            const match = findBestMatch(role, roles)
            if(match.bestMatch.rating > 0) {
                const displayName = match.bestMatch.target

                return cache.get(indexes[roles.indexOf(displayName)])
            }
        }
        return null
    }

    /**
	 * 
	 * @param {Guild} guild 
	 * @param {String} member
     * @param {Boolean} appro
	 */
    client.getMember = async(guild, member, appro) => {
        
        const { cache } = guild.members
        if (cache.has(member)) return cache.get(member)

        let mention = member.replace('<@','').replace('<@!','').replace('>')
        if (cache.has(mention)) return cache.get(mention)

        let result = cache.find(u => u.name === member)
        if (result) return result
        if (appro) {
            let members = []
            let indexes = []

            cache.forEach(member => {
                members.push(member.name)
                indexes.push(member.id)
            })

            const match = findBestMatch(member, members)
            if(match.bestMatch.rating > 0) {
                const displayName = match.bestMatch.target

                return cache.get(indexes[members.indexOf(displayName)])
            } 
        }
        return null
    }

    
    /**
	 * 
	 * @param {Message} message 
	 * @param {String} string
	 */
     client.succes = async(channel, string) => {
        channel.send(`${client.config.emojis.succes} • ${string}`)
    }

    
    /**
	 * 
	 * @param {Message} message 
	 * @param {String} string
	 */
     client.fail = async(channel, string) => {
        channel.send(`${client.config.emojis.fail} • ${string}`)
    }

    
    /**
	 * 
	 * @param {Number} ms
	 */
    client.sleep = async (ms) => {
    return new Promise((resolve) => {
        setTimeout(resolve, ms);
    });
    } 

    return client
}
